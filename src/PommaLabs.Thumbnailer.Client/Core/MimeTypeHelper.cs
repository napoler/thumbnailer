﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Client.Core;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using PommaLabs.MimeTypes;

/// <summary>
///   Helper which extends <see cref="MimeTypeMap"/>.
/// </summary>
internal static class MimeTypeHelper
{
    /// <summary>
    ///   Mime type deduced from file signatures can usually be trusted. However, "text/plain" can
    ///   be too generic because we might get better results by looking at file extension. Same
    ///   thing happens for "image/tiff" and other mime types below.
    /// </summary>
    private static readonly HashSet<string> s_tooGenericMimeTypes = new()
    {
        MimeTypeMap.APPLICATION.OCTET_STREAM,
        MimeTypeMap.APPLICATION.X_ZIP_COMPRESSED,
        MimeTypeMap.APPLICATION.ZIP,
        MimeTypeMap.IMAGE.TIFF,
        MimeTypeMap.TEXT.PLAIN
    };

    /// <summary>
    ///   <para>
    ///     Retrieves the mime type of specified file, deducing it from file signature and file name.
    ///   </para>
    ///   <para>
    ///     If multiple mime types match, then those starting with "image/" or "video/" are
    ///     preferred, because it is more likely that they will work with GraphicsMagick.
    ///   </para>
    ///   <para>If no match is found, then "application/octet-stream" is returned.</para>
    /// </summary>
    /// <param name="contents">File bytes.</param>
    /// <param name="fileName">File name.</param>
    /// <returns>The mime type deduced from file signature and file name.</returns>
    public static (string mimeType, string extension) GetMimeTypeAndExtension(byte[] contents, string? fileName)
    {
        using var memoryStream = new MemoryStream(contents);

        // "image/*" and "video/*" mime types are preferred in case of doubt, because they are more
        // likely to be correctly handled by the thumbnail generation process.
        var mimeTypeFromFileStream = MimeTypeMap.GetMimeTypes(memoryStream)
            .OrderByDescending(mt => mt.StartsWith("image/", StringComparison.OrdinalIgnoreCase))
            .ThenByDescending(mt => mt.StartsWith("video/", StringComparison.OrdinalIgnoreCase))
            .FirstOrDefault() ?? MimeTypeMap.APPLICATION.OCTET_STREAM;

        return !s_tooGenericMimeTypes.Contains(mimeTypeFromFileStream)
            ? (mimeTypeFromFileStream, MimeTypeMap.GetExtension(mimeTypeFromFileStream))
            : GetMimeTypeAndExtensionHelper(fileName, mimeTypeFromFileStream);
    }

    /// <summary>
    ///   <para>Retrieves the mime type linked to the extension of <paramref name="filePath"/>.</para>
    ///   <para>
    ///     If multiple mime types match, then those starting with "image/" or "video/" are
    ///     preferred, because it is more likely that they will work with GraphicsMagick.
    ///   </para>
    ///   <para>If the extension is not mapped, then "application/octet-stream" is returned.</para>
    /// </summary>
    /// <param name="filePath">File path.</param>
    /// <param name="contentTypeHint">Optional hint about file content type.</param>
    /// <returns>The mime type linked to the extension of <paramref name="filePath"/>.</returns>
    public static (string mimeType, string extension) GetMimeTypeAndExtension(string filePath, string? contentTypeHint = null)
    {
        var mimeTypeFromFileStream = contentTypeHint ?? MimeTypeMap.APPLICATION.OCTET_STREAM;

        if (File.Exists(filePath) && new FileInfo(filePath).Length > 0)
        {
            using var fileStream = File.OpenRead(filePath);

            // "image/*" and "video/*" mime types are preferred in case of doubt, because they are
            // more likely to be correctly handled by the thumbnail generation process.
            mimeTypeFromFileStream = MimeTypeMap.GetMimeTypes(fileStream)
                .OrderByDescending(mt => mt.StartsWith("image/", StringComparison.OrdinalIgnoreCase))
                .ThenByDescending(mt => mt.StartsWith("video/", StringComparison.OrdinalIgnoreCase))
                .FirstOrDefault() ?? mimeTypeFromFileStream;

            if (!s_tooGenericMimeTypes.Contains(mimeTypeFromFileStream))
            {
                // Extension might not be deduced from mime type when it is not mapped. Therefore,
                // we need to fall back to original file extension.
                var extension = MimeTypeMap.TryGetExtension(mimeTypeFromFileStream, out var ext) ? ext : Path.GetExtension(filePath);
                return (mimeTypeFromFileStream, extension);
            }
        }

        return GetMimeTypeAndExtensionHelper(filePath, mimeTypeFromFileStream);
    }

    private static (string mimeType, string extension) GetMimeTypeAndExtensionHelper(
        string? filePath, string mimeTypeFromFileStream)
    {
        if (string.IsNullOrWhiteSpace(filePath) ||
            string.IsNullOrWhiteSpace(Path.GetExtension(filePath)))
        {
            // File path is empty or without extension. Therefore, there is not much we can do.
            return (mimeTypeFromFileStream, MimeTypeMap.GetExtension(mimeTypeFromFileStream));
        }

        var extension = Path.GetExtension(filePath);

        // "image/*" and "video/*" mime types are preferred in case of doubt, because they are more
        // likely to be correctly handled by the thumbnail generation process.
        var mimeTypeFromFilePath = MimeTypeMap
            .GetMimeTypes(filePath!)
            .OrderByDescending(mt => mt.StartsWith("image/", StringComparison.OrdinalIgnoreCase))
            .ThenByDescending(mt => mt.StartsWith("video/", StringComparison.OrdinalIgnoreCase))
            .FirstOrDefault();

        if (mimeTypeFromFilePath == null)
        {
            if (mimeTypeFromFileStream == MimeTypeMap.TEXT.PLAIN)
            {
                // This might be a source code file. Source code mime type is usually undefined, but
                // their extension is needed for colored output.
                return (mimeTypeFromFileStream, extension);
            }

            // No valid mime type was found using file path. Therefore, we need to fall back to mime
            // type deduced from file signature.
            return (mimeTypeFromFileStream, MimeTypeMap.GetExtension(mimeTypeFromFileStream));
        }

        // A valid match was found and it is used instead of the one found by looking at file signature.
        return (mimeTypeFromFilePath, MimeTypeMap.GetExtension(mimeTypeFromFilePath));
    }
}
