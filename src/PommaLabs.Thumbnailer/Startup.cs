﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer;

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using AspNetCore.Authentication.ApiKey;
using Hellang.Middleware.ProblemDetails;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning.Conventions;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MySqlConnector;
using NodaTime;
using Npgsql;
using PommaLabs.KVLite.AspNetCore;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.Enumerations;
using PommaLabs.Thumbnailer.Models.Exceptions;
using PommaLabs.Thumbnailer.Services.ApiKeyProviders;
using PommaLabs.Thumbnailer.Services.AuthenticationHandlers;
using PommaLabs.Thumbnailer.Services.Background;
using PommaLabs.Thumbnailer.Services.ConfigureOptions;
using PommaLabs.Thumbnailer.Services.HealthChecks;
using PommaLabs.Thumbnailer.Services.Managers.Download;
using PommaLabs.Thumbnailer.Services.Managers.Job;
using PommaLabs.Thumbnailer.Services.Managers.Optimization;
using PommaLabs.Thumbnailer.Services.Managers.Process;
using PommaLabs.Thumbnailer.Services.Managers.Thumbnail;
using PommaLabs.Thumbnailer.Services.Middlewares;
using PommaLabs.Thumbnailer.Services.Stores.ApiKeys;
using PommaLabs.Thumbnailer.Services.Stores.TempFiles;
using PommaLabs.Thumbnailer.Services.TelemetryProcessors;
using reCAPTCHA.AspNetCore;
using Serilog;
using SqlKata.Compilers;
using SqlKata.Execution;
using Swashbuckle.AspNetCore.SwaggerGen;

/// <summary>
///   Represents the startup process for the application.
/// </summary>
public sealed class Startup
{
    private static readonly string s_informationalVersion = typeof(Startup).Assembly
        .GetCustomAttribute<AssemblyInformationalVersionAttribute>()
        ?.InformationalVersion ?? "0.0.0";

    /// <summary>
    ///   Initializes a new instance of the <see cref="Startup"/> class.
    /// </summary>
    /// <param name="configuration">The current configuration.</param>
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    /// <summary>
    ///   Gets the current configuration.
    /// </summary>
    /// <value>The current application configuration.</value>
    public IConfiguration Configuration { get; }

    /// <summary>
    ///   Configures the application using the provided builder, hosting environment, and API
    ///   version description provider.
    /// </summary>
    /// <param name="app">The current application builder.</param>
    /// <param name="provider">
    ///   The API version descriptor provider used to enumerate defined API versions.
    /// </param>
    /// <param name="websiteConfiguration">Website configuration.</param>
    public void Configure(
        IApplicationBuilder app, IApiVersionDescriptionProvider provider,
        IOptions<WebsiteConfiguration> websiteConfiguration)
    {
        var websiteEnabled = websiteConfiguration.Value.Enabled;

        // Security headers:
        if (websiteEnabled)
        {
            app.UseReferrerPolicy(options => options.NoReferrer());
            app.UseXContentTypeOptions();
            app.UseXDownloadOptions();
            app.UseXfo(options => options.Deny());
            app.UseXXssProtection(options => options.EnabledWithBlockMode());
            app.UseCsp(options => options
                .DefaultSources(c => c.None())
                .ConnectSources(c => c.Self())
                .FontSources(c => c.Self().CustomSources("fonts.gstatic.com"))
                .FormActions(c => c.Self())
                .FrameSources(c => c.CustomSources("www.recaptcha.net"))
                .ImageSources(c => c.Self().CustomSources("data:", "www.gstatic.com"))
                .ScriptSources(c => c.Self().UnsafeInline().CustomSources("www.recaptcha.net", "www.gstatic.com"))
                .StyleSources(c => c.Self().UnsafeInline().CustomSources("fonts.googleapis.com")));
        }
        else
        {
            app.UseXRobotsTag(options => options.NoIndex().NoFollow());
        }

        var redirectRules = new RewriteOptions();
        redirectRules.AddRedirect("^docs\\/?$", "swagger");
        if (!websiteEnabled)
        {
            // When website is not enabled, redirect root path to Swagger docs.
            redirectRules.AddRedirect("^$", "swagger");
        }
        app.UseRewriter(redirectRules);

        app.UseSwagger();
        app.UseSwaggerUI(c =>
        {
            // Build a swagger endpoint for each discovered API version:
            foreach (var groupName in provider.ApiVersionDescriptions.OrderByDescending(vd => vd.ApiVersion).Select(vd => vd.GroupName))
            {
                c.SwaggerEndpoint($"/swagger/{groupName}/swagger.json", groupName.ToUpperInvariant());
            }
        });

        if (websiteEnabled)
        {
            // Expose static files only when website has been enabled, because they are not needed otherwise.
            app.UseStaticFiles();
        }

        app.UseProblemDetails();

        // Request logging middleware MUST be placed AFTER all middlewares which should not be logged.
        app.UseSerilogRequestLogging();

        app.UseRouting();

        app.UseAuthentication();
        app.UseAuthorization();

        app.UseMiddleware<LogApiKeyNameMiddleware>();

        app.UseNoCacheHttpHeaders();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapHealthChecks(Constants.HealthChecksEndpoint, new HealthCheckOptions { ResponseWriter = WriteHealthCheckResponse }).WithMetadata(new AllowAnonymousAttribute());
            endpoints.MapGet(Constants.VersionEndpoint, async context => await context.Response.WriteAsJsonAsync(s_informationalVersion));
            endpoints.MapControllers();

            if (websiteEnabled)
            {
                // Map Razor Pages only when website has been enabled, in order not to overlap with
                // the "redirect to Swagger" rule.
                endpoints.MapRazorPages();
            }
        });
    }

    /// <summary>
    ///   Configures services for the application.
    /// </summary>
    /// <param name="services">The collection of services to configure the application with.</param>
    public void ConfigureServices(IServiceCollection services)
    {
        // Configuration classes:
        services.Configure<DatabaseConfiguration>(Configuration.GetSection("Database"));
        services.Configure<JobProcessorConfiguration>(Configuration.GetSection("JobProcessor"));
        services.Configure<RecaptchaSettings>(Configuration.GetSection("Website").GetSection("Recaptcha"));
        services.Configure<SecurityConfiguration>(Configuration.GetSection("Security"));
        services.Configure<WebsiteConfiguration>(Configuration.GetSection("Website"));

        var databaseConfiguration = Configuration.GetSection("Database").Get<DatabaseConfiguration>() ?? new DatabaseConfiguration();
        var jobProcessorConfiguration = Configuration.GetSection("JobProcessor").Get<JobProcessorConfiguration>() ?? new JobProcessorConfiguration();
        var securityConfiguration = Configuration.GetSection("Security").Get<SecurityConfiguration>() ?? new SecurityConfiguration();

        // API keys:
        switch (databaseConfiguration.Provider)
        {
            case DatabaseProvider.MySql:
                services.AddScoped(_ =>
                {
                    var connection = new MySqlConnection(databaseConfiguration.ConnectionString);
                    var compiler = new MySqlCompiler();
                    return new QueryFactory(connection, compiler);
                });
                break;

            case DatabaseProvider.PostgreSql:
                services.AddScoped(_ =>
                {
                    var connection = new NpgsqlConnection(databaseConfiguration.ConnectionString);
                    var compiler = new PostgresCompiler();
                    return new QueryFactory(connection, compiler);
                });
                break;

            case DatabaseProvider.SqlServer:
                services.AddScoped(_ =>
                {
                    var connection = new SqlConnection(databaseConfiguration.ConnectionString);
                    var compiler = new SqlServerCompiler();
                    return new QueryFactory(connection, compiler);
                });
                break;
        }

        if (databaseConfiguration.Provider != DatabaseProvider.None)
        {
            services.AddScoped<IApiKeyStore, DbApiKeyStore>();
        }
        else
        {
            services.AddSingleton<IApiKeyStore, MemoryApiKeyStore>();
        }
        services.Decorate<IApiKeyStore, ConfigurationApiKeyStore>();
        services.Decorate<IApiKeyStore, CachingApiKeyStore>();

        // KVLite cache:
        const bool AutoCreateCacheSchema = false;
        const string CacheEntriesTableName = "cache_entries";
        switch (databaseConfiguration.Provider)
        {
            case DatabaseProvider.MySql:
                services.AddKVLiteMySqlCache(options =>
                {
                    options.AutoCreateCacheSchema = AutoCreateCacheSchema;
                    options.ConnectionString = databaseConfiguration.ConnectionString;
                    options.CacheEntriesTableName = CacheEntriesTableName;
                    options.CacheSchemaName = databaseConfiguration.SchemaName;
                });
                break;

            case DatabaseProvider.PostgreSql:
                services.AddKVLitePostgreSqlCache(options =>
                {
                    options.AutoCreateCacheSchema = AutoCreateCacheSchema;
                    options.ConnectionString = databaseConfiguration.ConnectionString;
                    options.CacheEntriesTableName = CacheEntriesTableName;
                    options.CacheSchemaName = databaseConfiguration.SchemaName;
                });
                break;

            case DatabaseProvider.SqlServer:
                services.AddKVLiteSqlServerCache(options =>
                {
                    options.AutoCreateCacheSchema = AutoCreateCacheSchema;
                    options.ConnectionString = databaseConfiguration.ConnectionString;
                    options.CacheEntriesTableName = CacheEntriesTableName;
                    options.CacheSchemaName = databaseConfiguration.SchemaName;
                });
                break;

            default:
                services.AddKVLiteMemoryCache();
                break;
        }

        // ASP.NET Core:
        services.AddProblemDetails(options =>
        {
            options.MapToStatusCode<ArgumentException>(StatusCodes.Status400BadRequest);
            options.MapToStatusCode<ArgumentNullException>(StatusCodes.Status400BadRequest);
            options.MapToStatusCode<ProcessException>(StatusCodes.Status422UnprocessableEntity);
            options.MapToStatusCode<InvalidOperationException>(StatusCodes.Status424FailedDependency);
            options.MapToStatusCode<NotSupportedException>(StatusCodes.Status403Forbidden);
            options.MapToStatusCode<Exception>(StatusCodes.Status500InternalServerError);
        });

        services.AddControllers()
            .AddJsonOptions(options => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));

        services.Configure<FormOptions>(options => options.MultipartBodyLengthLimit = securityConfiguration.MaxFileUploadSizeInBytes);

        services.PostConfigure<ApiBehaviorOptions>(options =>
        {
            var builtInFactory = options.InvalidModelStateResponseFactory;

            options.InvalidModelStateResponseFactory = context =>
            {
                var loggerFactory = context.HttpContext.RequestServices.GetRequiredService<ILoggerFactory>();
                var logger = loggerFactory.CreateLogger(context.ActionDescriptor.DisplayName!);

                var modelValidationErrors = context.ModelState
                    .Where(x => x.Value?.Errors?.Any() ?? false)
                    .SelectMany(x => x.Value!.Errors.Select(e => new { FieldName = x.Key, Error = e }))
                    .ToList();

                foreach (var mve in modelValidationErrors)
                {
                    logger.LogWarning(mve.Error.Exception, "Field {FieldName} is not valid: {ValidationErrorMessage}", mve.FieldName, mve.Error.ErrorMessage);
                }

                return builtInFactory(context);
            };
        });

        services.AddApiVersioning(options =>
        {
            // Reporting API versions will return the headers "api-supported-versions" and "api-deprecated-versions".
            options.ReportApiVersions = true;

            // Automatically applies an API version based on the name of the defining controller's namespace.
            options.Conventions.Add(new VersionByNamespaceConvention());
        });

        services.AddVersionedApiExplorer(options =>
        {
            // Adds the versioned API explorer, which also adds IApiVersionDescriptionProvider service.
            // Note: the specified format code will format the version as "'v'major[.minor][-status]".
            options.GroupNameFormat = "'v'VVV";

            // Note: this option is only necessary when versioning by URL segment. The SubstitutionFormat
            // can also be used to control the format of the API version in route templates.
            options.SubstituteApiVersionInUrl = true;
        });

        services.AddHttpContextAccessor();

        services.AddDataProtection()
            .PersistKeysToFileSystem(new DirectoryInfo(Constants.TempDirectory + "/data-protection-keys"));

        // Razor Pages:
        services.AddRazorPages().AddRazorRuntimeCompilation();

        // Swagger UI:
        services.AddSwaggerGen();
        services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerGenOptions>();

        // Health checks:
        services.AddHealthChecks()
            .AddCheck<ApiKeyStoreHealthCheck>(nameof(ApiKeyStoreHealthCheck))
            .AddCheck<KVLiteHealthCheck>(nameof(KVLiteHealthCheck))
            .AddCheck<TempFileStoreHealthCheck>(nameof(TempFileStoreHealthCheck));

        // Application Insights:
        services.AddApplicationInsightsTelemetry();
        services.AddApplicationInsightsTelemetryProcessor<ProbeFilter>();

        // Recaptcha:
        services.AddTransient<IRecaptchaService, RecaptchaService>();

        // Clock:
        services.AddSingleton<IClock>(NodaTime.SystemClock.Instance);

        // Authentication handlers:
        var authenticationSchemes = new List<string>();
        var authenticationBuilder = services.AddAuthentication();

        services.AddScoped<InternalApiKeyProvider>();
        if (securityConfiguration.AcceptApiKeysViaHeaderParameter)
        {
            authenticationSchemes.Add(Constants.ApiKeyViaHeaderAuthenticationScheme);
            authenticationBuilder.AddApiKeyInHeader<InternalApiKeyProvider>(Constants.ApiKeyViaHeaderAuthenticationScheme, options =>
            {
                options.Realm = nameof(Thumbnailer);
                options.KeyName = Constants.ApiKeyHeaderName;
                options.IgnoreAuthenticationIfAllowAnonymous = true;
            });
        }
        if (securityConfiguration.AcceptApiKeysViaQueryStringParameter)
        {
            authenticationSchemes.Add(Constants.ApiKeyViaQueryParameterAuthenticationScheme);
            authenticationBuilder.AddApiKeyInQueryParams<InternalApiKeyProvider>(Constants.ApiKeyViaQueryParameterAuthenticationScheme, options =>
            {
                options.Realm = nameof(Thumbnailer);
                options.KeyName = Constants.ApiKeyQueryParameterName;
                options.IgnoreAuthenticationIfAllowAnonymous = true;
            });
        }

        if (securityConfiguration.AllowAnonymousAccess)
        {
            authenticationSchemes.Add(Constants.AnonymousAuthenticationScheme);
            authenticationBuilder.AddScheme<AuthenticationSchemeOptions, AnonymousAuthenticationHandler>(Constants.AnonymousAuthenticationScheme, default);
        }

        services.AddAuthorization(options =>
        {
            // Allow multiple authorization schemes:
            var defaultAuthorizationPolicyBuilder = new AuthorizationPolicyBuilder(authenticationSchemes.ToArray());

            defaultAuthorizationPolicyBuilder = defaultAuthorizationPolicyBuilder.RequireAuthenticatedUser();
            options.DefaultPolicy = defaultAuthorizationPolicyBuilder.Build();
        });

        // Background:
        services.AddHostedService<CleanupTempApiKeysBackgroundService>();
        services.AddHostedService<CleanupTempFilesBackgroundService>();

        for (var i = 1; i <= jobProcessorConfiguration.Count; ++i)
        {
            var processorId = i;
            services.AddSingleton<IHostedService>(sp =>
            {
                return new ProcessQueuedJobsBackgroundService(
                    processorId: processorId,
                    logger: sp.GetRequiredService<ILogger<ProcessQueuedJobsBackgroundService>>(),
                    jobManager: sp.GetRequiredService<IJobManager>(),
                    optimizationManager: sp.GetRequiredService<IOptimizationManager>(),
                    thumbnailManager: sp.GetRequiredService<IThumbnailManager>());
            });
        }

        // Managers:
        services.AddSingleton<IProcessManager, SystemProcessManager>();

        services.AddSingleton<IDownloadManager, HttpDownloadManager>();
        services.Decorate<IDownloadManager, CachingDownloadManager>();
        services.Decorate<IDownloadManager, ValidatingDownloadManager>();

        services.AddSingleton<IJobManager, ConcurrentJobManager>();
        services.Decorate<IJobManager, ValidatingJobManager>();

        services.AddSingleton<IOptimizationManager, ConcreteOptimizationManager>();
        services.Decorate<IOptimizationManager, CachingOptimizationManager>();
        services.Decorate<IOptimizationManager, TracingOptimizationManager>();
        services.Decorate<IOptimizationManager, ValidatingOptimizationManager>();

        services.AddSingleton<IThumbnailManager, ConcreteThumbnailManager>();
        services.Decorate<IThumbnailManager, CachingThumbnailManager>();
        services.Decorate<IThumbnailManager, TracingThumbnailManager>();
        services.Decorate<IThumbnailManager, ValidatingThumbnailManager>();

        // Stores:
        services.AddSingleton<ITempFileStore, SystemTempFileStore>();
        services.Decorate<ITempFileStore, EnrichingTempFileStore>();
        services.Decorate<ITempFileStore, CachingTempFileStore>();
    }

    private static async Task WriteHealthCheckResponse(HttpContext context, HealthReport result)
    {
        context.Response.ContentType = "application/json; charset=utf-8";

        using var stream = new MemoryStream();
        using (var writer = new Utf8JsonWriter(stream))
        {
            writer.WriteStartObject();
            writer.WriteString("status", result.Status.ToString());
            writer.WriteNumber("totalDuration", result.TotalDuration.TotalSeconds);
            writer.WriteStartObject("results");
            foreach (var entry in result.Entries)
            {
                writer.WriteStartObject(entry.Key);
                writer.WriteString("status", entry.Value.Status.ToString());
                writer.WriteString("description", entry.Value.Description);
                writer.WriteNumber("duration", entry.Value.Duration.TotalSeconds);
                writer.WriteStartObject("data");
                foreach (var item in entry.Value.Data)
                {
                    writer.WritePropertyName(item.Key);
                    JsonSerializer.Serialize(writer, item.Value, item.Value?.GetType() ?? typeof(object));
                }
                writer.WriteEndObject();
                writer.WriteEndObject();
            }
            writer.WriteEndObject();
            writer.WriteEndObject();
        }

        var json = Encoding.UTF8.GetString(stream.ToArray());
        await context.Response.WriteAsync(json);
    }
}
