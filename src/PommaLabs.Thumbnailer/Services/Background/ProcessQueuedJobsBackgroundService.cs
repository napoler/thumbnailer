﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Background;

using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Services.Managers.Job;
using PommaLabs.Thumbnailer.Services.Managers.Optimization;
using PommaLabs.Thumbnailer.Services.Managers.Thumbnail;

/// <summary>
///   Background service which processes asynchronous jobs.
/// </summary>
public sealed class ProcessQueuedJobsBackgroundService : BackgroundService
{
    private readonly IJobManager _jobManager;
    private readonly ILogger<ProcessQueuedJobsBackgroundService> _logger;
    private readonly IOptimizationManager _optimizationManager;
    private readonly int _processorId;
    private readonly IThumbnailManager _thumbnailManager;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="processorId">Processor ID.</param>
    /// <param name="logger">Logger.</param>
    /// <param name="jobManager">Job manager.</param>
    /// <param name="optimizationManager">Optimization manager.</param>
    /// <param name="thumbnailManager">Thumbnail manager.</param>
    public ProcessQueuedJobsBackgroundService(
        int processorId,
        ILogger<ProcessQueuedJobsBackgroundService> logger,
        IJobManager jobManager,
        IOptimizationManager optimizationManager,
        IThumbnailManager thumbnailManager)
    {
        _processorId = processorId;
        _logger = logger;
        _jobManager = jobManager;
        _optimizationManager = optimizationManager;
        _thumbnailManager = thumbnailManager;
    }

    /// <inheritdoc/>
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("Queued jobs processor service with ID {ProcessorId} is starting", _processorId);

        while (!stoppingToken.IsCancellationRequested)
        {
            ThumbnailerCommandBase? command = null;
            try
            {
                command = await _jobManager.DequeueCommandAsync(stoppingToken);
                command.ProcessorId = _processorId;

                var result = command switch
                {
                    MediaOptimizationCommand moCommand => await _optimizationManager.OptimizeMediaAsync(moCommand, stoppingToken),
                    ThumbnailGenerationCommand tgCommand => await _thumbnailManager.GenerateThumbnailAsync(tgCommand, stoppingToken),
                    _ => throw new InvalidOperationException($"Given command type '{command.GetType().Name}' does not have an handler"),
                };

                _logger.LogDebug("Command with internal job ID '{InternalJobId}' was processed successfully", command.InternalJobId);
                await _jobManager.MarkJobAsProcessedAsync(command.InternalJobId.GetValueOrDefault(), result, stoppingToken);
            }
            catch (Exception ex)
            {
                if (command == null)
                {
                    _logger.LogWarning(ex, "An error occurred while retrieving a new command to be processed");
                    continue;
                }

                _logger.LogWarning(ex, "An error occurred while processing a command with internal job ID '{InternalJobId}'", command.InternalJobId);
                await _jobManager.MarkJobAsFailedAsync(command.InternalJobId.GetValueOrDefault(), ex.Message, stoppingToken);
            }
        }

        _logger.LogInformation("Queued jobs processor service with ID {ProcessorId} is stopping", _processorId);
    }
}
