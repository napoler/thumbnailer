﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.ConfigureOptions;

using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Services.OperationFilters;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.SwaggerGen;

/// <summary>
///   Configures the Swagger generation options.
/// </summary>
/// <remarks>
///   This allows API versioning to define a Swagger document per API version after the
///   <see cref="IApiVersionDescriptionProvider"/> service has been resolved from the service container.
/// </remarks>
public sealed class ConfigureSwaggerGenOptions : IConfigureOptions<SwaggerGenOptions>
{
    private readonly IApiVersionDescriptionProvider _provider;
    private readonly IOptions<SecurityConfiguration> _securityConfiguration;

    /// <summary>
    ///   Initializes a new instance of the <see cref="ConfigureSwaggerGenOptions"/> class.
    /// </summary>
    /// <param name="provider">
    ///   The <see cref="IApiVersionDescriptionProvider">provider</see> used to generate Swagger documents.
    /// </param>
    /// <param name="securityConfiguration">Security configuration.</param>
    public ConfigureSwaggerGenOptions(
        IApiVersionDescriptionProvider provider,
        IOptions<SecurityConfiguration> securityConfiguration)
    {
        _provider = provider;
        _securityConfiguration = securityConfiguration;
    }

    private static string XmlCommentsFilePath
    {
        get
        {
            var fileName = typeof(Startup).GetTypeInfo().Assembly.GetName().Name + ".xml";
            return Path.Combine(AppContext.BaseDirectory, fileName);
        }
    }

    /// <inheritdoc/>
    public void Configure(SwaggerGenOptions options)
    {
        // Adds a Swagger document for each discovered API version.
        // Note: you might choose to skip or document deprecated API versions differently.
        foreach (var description in _provider.ApiVersionDescriptions)
        {
            options.SwaggerDoc(description.GroupName, CreateInfoForApiVersion(description));
        }

        // Integrate XML comments:
        options.IncludeXmlComments(XmlCommentsFilePath);

        options.OperationFilter<BinarySchemaOperationFilter>();
        options.OperationFilter<DeprecatedOperationFilter>();

        options.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();
        options.OperationFilter<SecurityRequirementsOperationFilter>(false, "api_key_header");
        options.OperationFilter<SecurityRequirementsOperationFilter>(false, "api_key_query");

        if (_securityConfiguration.Value.AcceptApiKeysViaHeaderParameter)
        {
            options.AddSecurityDefinition("api_key_header", new OpenApiSecurityScheme
            {
                Description = $"Custom \"{Constants.ApiKeyHeaderName}\" header.",
                Type = SecuritySchemeType.ApiKey,
                In = ParameterLocation.Header,
                Name = Constants.ApiKeyHeaderName
            });
        }
        if (_securityConfiguration.Value.AcceptApiKeysViaQueryStringParameter)
        {
            options.AddSecurityDefinition("api_key_query", new OpenApiSecurityScheme
            {
                Description = $"Custom \"{Constants.ApiKeyQueryParameterName}\" query.",
                Type = SecuritySchemeType.ApiKey,
                In = ParameterLocation.Query,
                Name = Constants.ApiKeyQueryParameterName
            });
        }
    }

    [SuppressMessage("Minor Code Smell", "S1075:URIs should not be hardcoded", Justification = "External URI of MIT license")]
    private static OpenApiInfo CreateInfoForApiVersion(ApiVersionDescription description)
    {
        var info = new OpenApiInfo
        {
            Title = $"Thumbnailer API {description.GroupName}",
            Version = description.ApiVersion.ToString(),
            Description = "File thumbnail generator and simple media file optimizer.",
            License = new OpenApiLicense { Name = "MIT", Url = new Uri("https://opensource.org/licenses/MIT") }
        };

        if (description.IsDeprecated)
        {
            info.Description += " This API version has been deprecated.";
        }

        return info;
    }
}
