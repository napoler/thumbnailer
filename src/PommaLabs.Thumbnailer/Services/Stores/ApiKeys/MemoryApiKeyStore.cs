﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Stores.ApiKeys;

using System;
using System.Collections.Concurrent;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using NodaTime;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   An API key store which stores temporary API keys in memory.
/// </summary>
public sealed class MemoryApiKeyStore : ApiKeyStoreBase
{
    private readonly ConcurrentDictionary<string, ApiKeyConfiguration> _apiKeys = new();
    private readonly IClock _clock;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="securityConfiguration">Security configuration.</param>
    /// <param name="clock">Clock.</param>
    public MemoryApiKeyStore(
        IOptions<SecurityConfiguration> securityConfiguration,
        IClock clock)
        : base(securityConfiguration)
    {
        _clock = clock;
    }

    /// <inheritdoc/>
    public override Task<ApiKeyCredentials> AddTempApiKeyAsync(CancellationToken cancellationToken)
    {
        var utcNow = _clock.GetCurrentInstant().ToDateTimeOffset();
        var tempApiKey = new ApiKeyConfiguration
        {
            Name = $"TMP#{utcNow.ToUnixTimeSeconds()}",
            Value = Guid.NewGuid().ToString("N", CultureInfo.InvariantCulture).ToUpperInvariant(),
            ExpiresAt = utcNow.AddMinutes(15)
        };

        _apiKeys.TryAdd(tempApiKey.Value, tempApiKey);

        return Task.FromResult(new ApiKeyCredentials
        {
            Name = tempApiKey.Name,
            Value = tempApiKey.Value,
            IsValid = true
        });
    }

    /// <inheritdoc/>
    public override Task<int> DeleteExpiredTempApiKeysAsync(CancellationToken cancellationToken)
    {
        var expiredApiKeys = _apiKeys.Values
            .Where(x => x.ExpiresAt < _clock.GetCurrentInstant().ToDateTimeOffset())
            .Select(x => x.Value)
            .ToList();

        var deletedCount = 0;
        foreach (var ak in expiredApiKeys)
        {
            deletedCount += _apiKeys.TryRemove(ak, out _) ? 1 : 0;
        }

        return Task.FromResult(deletedCount);
    }

    /// <inheritdoc/>
    public override Task<ApiKeyCredentials?> ValidateApiKeyAsync(string apiKey, CancellationToken cancellationToken)
    {
        return Task.FromResult(_apiKeys.Values
            .Where(x => ApiKeysMatch(x.Value, apiKey) && x.ExpiresAt >= _clock.GetCurrentInstant().ToDateTimeOffset())
            .Select(x => new ApiKeyCredentials { Name = x.Name, Value = x.Value, IsValid = true })
            .SingleOrDefault());
    }
}
