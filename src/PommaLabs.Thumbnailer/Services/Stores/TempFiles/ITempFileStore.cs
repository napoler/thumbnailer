﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Stores.TempFiles;

using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.DTO.Public;

/// <summary>
///   Simple file system abstraction for temporary files.
/// </summary>
public interface ITempFileStore
{
    /// <summary>
    ///   Deletes specified temporary file.
    /// </summary>
    /// <param name="file">Temporary file.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    Task DeleteTempFileAsync(TempFileMetadata file, CancellationToken cancellationToken);

    /// <summary>
    ///   Get the available free space, in bytes, on the drive where temporary directory is mounted.
    /// </summary>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>
    ///   The available free space, in bytes, on the drive where temporary directory is mounted.
    /// </returns>
    Task<long> GetAvailableFreeSpaceInBytes(CancellationToken cancellationToken);

    /// <summary>
    ///   Creates a temporary file, using given <paramref name="contentType"/> to determine its extension.
    /// </summary>
    /// <param name="contentType">Content type.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>
    ///   A new temporary file, using given <paramref name="contentType"/> to determine its extension.
    /// </returns>
    Task<TempFileMetadata> GetTempFileAsync(string contentType, CancellationToken cancellationToken) => GetTempFileAsync(contentType, null, cancellationToken);

    /// <summary>
    ///   Creates a temporary file, using given <paramref name="contentType"/> and
    ///   <paramref name="extension"/> to determine its extension.
    /// </summary>
    /// <param name="contentType">Content type.</param>
    /// <param name="extension">File extension.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>
    ///   A new temporary file, using given <paramref name="contentType"/> and
    ///   <paramref name="extension"/> to determine its extension.
    /// </returns>
    Task<TempFileMetadata> GetTempFileAsync(string contentType, string? extension, CancellationToken cancellationToken);

    /// <summary>
    ///   Lists all temporary files.
    /// </summary>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An enumerable with all temporary files.</returns>
    Task<IEnumerable<TempFileMetadata>> GetTempFilesAsync(CancellationToken cancellationToken);

    /// <summary>
    ///   Checks whether downloaded file is already available or not among temporary files.
    /// </summary>
    /// <param name="downloadedFile">Downloaded file.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>
    ///   The same file, if a local copy was not found. Otherwise, it returns the existing local copy.
    /// </returns>
    Task<TempFileMetadata> HandleFileDownloadAsync(TempFileMetadata downloadedFile, CancellationToken cancellationToken);

    /// <summary>
    ///   Makes a local copy of given form file.
    /// </summary>
    /// <param name="formFile">Form file.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An internal structure representing the copied file.</returns>
    Task<TempFileMetadata> HandleFileUploadAsync(IFormFile formFile, CancellationToken cancellationToken);

    /// <summary>
    ///   Makes a local copy of given external file.
    /// </summary>
    /// <param name="uploadedFile">Uploaded file.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An internal structure representing the copied file.</returns>
    Task<TempFileMetadata> HandleFileUploadAsync(FileDetails uploadedFile, CancellationToken cancellationToken);

    /// <summary>
    ///   Updates <paramref name="file"/>, using given <paramref name="newContentType"/> and
    ///   <paramref name="newExtension"/> to determine its extension.
    /// </summary>
    /// <param name="file">File that should be updated.</param>
    /// <param name="newContentType">Content type.</param>
    /// <param name="newExtension">File extension.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>
    ///   An updated temporary file, using given <paramref name="newContentType"/> and
    ///   <paramref name="newExtension"/> to determine its extension.
    /// </returns>
    Task<TempFileMetadata> UpdateTempFileAsync(
        TempFileMetadata file, string newContentType, string newExtension,
        CancellationToken cancellationToken);
}
