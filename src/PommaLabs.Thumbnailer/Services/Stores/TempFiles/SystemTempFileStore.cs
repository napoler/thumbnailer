﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Stores.TempFiles;

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.DTO.Public;

/// <summary>
///   Concrete <see cref="ITempFileStore"/> implementation.
/// </summary>
public sealed class SystemTempFileStore : ITempFileStore
{
    private readonly ILogger<SystemTempFileStore> _logger;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="logger">Logger.</param>
    public SystemTempFileStore(ILogger<SystemTempFileStore> logger)
    {
        _logger = logger;

        if (!Directory.Exists(Constants.TempDirectory))
        {
            Directory.CreateDirectory(Constants.TempDirectory);
        }
    }

    /// <inheritdoc/>
    public Task DeleteTempFileAsync(TempFileMetadata file, CancellationToken cancellationToken)
    {
        File.Delete(file.Path);

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public Task<long> GetAvailableFreeSpaceInBytes(CancellationToken cancellationToken)
    {
        return Task.FromResult(new DriveInfo(Constants.TempDirectory).AvailableFreeSpace);
    }

    /// <inheritdoc/>
    public Task<TempFileMetadata> GetTempFileAsync(
        string contentType, string? extension,
        CancellationToken cancellationToken)
    {
        var tempFile = new TempFileMetadata(contentType, extension);
        tempFile.Path = Path.Combine(Constants.TempDirectory, Path.GetRandomFileName() + tempFile.Extension);

        File.Create(tempFile.Path).Dispose();

        return Task.FromResult(tempFile);
    }

    /// <inheritdoc/>
    public Task<IEnumerable<TempFileMetadata>> GetTempFilesAsync(CancellationToken cancellationToken)
    {
        return Task.FromResult<IEnumerable<TempFileMetadata>>(Directory
            .GetFiles(Constants.TempDirectory)
            .Select(x => new TempFileMetadata(MimeTypeMap.APPLICATION.OCTET_STREAM) { Path = x })
            .ToList());
    }

    /// <inheritdoc/>
    public Task<TempFileMetadata> HandleFileDownloadAsync(
        TempFileMetadata downloadedFile, CancellationToken cancellationToken)
    {
        return Task.FromResult(downloadedFile);
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> HandleFileUploadAsync(
        IFormFile formFile, CancellationToken cancellationToken)
    {
        var contentType = formFile.ContentType ?? MimeTypeMap.APPLICATION.OCTET_STREAM;
        var fileName = Helpers.CleanupFileName(formFile.FileName);
        var extension = Path.GetExtension(fileName);
        _logger.LogInformation("Handling upload of file with content type '{ContentType}', extension '{Extension}' and file name '{FileName}'", contentType, extension, fileName);

        var file = await GetTempFileAsync(contentType, extension, cancellationToken);
        if (file.ContentType != contentType)
        {
            _logger.LogInformation("Content type of uploaded file has been overridden with '{ContentType}'", contentType);
        }

        using (var fileStream = File.OpenWrite(file.Path))
        {
            await formFile.CopyToAsync(fileStream, cancellationToken);
        }
        _logger.LogDebug("File upload handled successfully, it has been stored at path '{FilePath}'", file.Path);

        return file;
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> HandleFileUploadAsync(
        FileDetails uploadedFile, CancellationToken cancellationToken)
    {
        var contentType = uploadedFile.ContentType ?? MimeTypeMap.APPLICATION.OCTET_STREAM;
        var fileName = Helpers.CleanupFileName(uploadedFile.FileName);
        var extension = Path.GetExtension(fileName);
        _logger.LogInformation("Handling upload of file with content type '{ContentType}', extension '{Extension}' and file name '{FileName}'", contentType, extension, fileName);

        var file = await GetTempFileAsync(contentType, extension, cancellationToken);
        if (file.ContentType != contentType)
        {
            _logger.LogInformation("Content type of uploaded file has been overridden with '{ContentType}'", contentType);
        }

        using (var fileStream = File.OpenWrite(file.Path))
        {
            using var contentStream = new MemoryStream(uploadedFile.Contents);
            await contentStream.CopyToAsync(fileStream, cancellationToken);
        }
        _logger.LogDebug("File upload handled successfully, it has been stored at path '{FilePath}'", file.Path);

        return file;
    }

    /// <inheritdoc/>
    public Task<TempFileMetadata> UpdateTempFileAsync(
        TempFileMetadata file, string newContentType, string newExtension,
        CancellationToken cancellationToken)
    {
        var newPath = Path.Combine(Constants.TempDirectory, Path.GetRandomFileName() + newExtension);

        File.Move(file.Path, newPath);

        return Task.FromResult(new TempFileMetadata(newContentType, newExtension) { Path = newPath });
    }
}
