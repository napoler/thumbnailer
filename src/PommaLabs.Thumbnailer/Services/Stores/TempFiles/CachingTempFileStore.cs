﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Stores.TempFiles;

using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using PommaLabs.KVLite;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.DTO.Public;

/// <summary>
///   Temporary file store which applies caching policies.
/// </summary>
public sealed class CachingTempFileStore : ITempFileStore
{
    private readonly ICache _cache;
    private readonly IOptions<DatabaseConfiguration> _databaseConfiguration;
    private readonly ITempFileStore _tempFileStore;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="tempFileStore">Temporary file store.</param>
    /// <param name="cache">Cache.</param>
    /// <param name="databaseConfiguration">Database configuration.</param>
    public CachingTempFileStore(
        ITempFileStore tempFileStore,
        ICache cache,
        IOptions<DatabaseConfiguration> databaseConfiguration)
    {
        _tempFileStore = tempFileStore;
        _cache = cache;
        _databaseConfiguration = databaseConfiguration;
    }

    /// <inheritdoc/>
    public Task DeleteTempFileAsync(TempFileMetadata file, CancellationToken cancellationToken)
    {
        return _tempFileStore.DeleteTempFileAsync(file, cancellationToken);
    }

    /// <inheritdoc/>
    public Task<long> GetAvailableFreeSpaceInBytes(CancellationToken cancellationToken)
    {
        return _tempFileStore.GetAvailableFreeSpaceInBytes(cancellationToken);
    }

    /// <inheritdoc/>
    public Task<TempFileMetadata> GetTempFileAsync(
        string contentType, string? extension,
        CancellationToken cancellationToken)
    {
        return _tempFileStore.GetTempFileAsync(contentType, extension, cancellationToken);
    }

    /// <inheritdoc/>
    public Task<IEnumerable<TempFileMetadata>> GetTempFilesAsync(CancellationToken cancellationToken)
    {
        return _tempFileStore.GetTempFilesAsync(cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> HandleFileDownloadAsync(
        TempFileMetadata downloadedFile, CancellationToken cancellationToken)
    {
        var file = await _tempFileStore.HandleFileDownloadAsync(downloadedFile, cancellationToken);
        return await CheckCacheAsync(file, cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> HandleFileUploadAsync(
        IFormFile formFile, CancellationToken cancellationToken)
    {
        var file = await _tempFileStore.HandleFileUploadAsync(formFile, cancellationToken);
        return await CheckCacheAsync(file, cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> HandleFileUploadAsync(
        FileDetails uploadedFile, CancellationToken cancellationToken)
    {
        var file = await _tempFileStore.HandleFileUploadAsync(uploadedFile, cancellationToken);
        return await CheckCacheAsync(file, cancellationToken);
    }

    /// <inheritdoc/>
    public Task<TempFileMetadata> UpdateTempFileAsync(
        TempFileMetadata file, string newContentType, string newExtension,
        CancellationToken cancellationToken)
    {
        return _tempFileStore.UpdateTempFileAsync(file, newContentType, newExtension, cancellationToken);
    }

    private static string ComputeSha384Hash(Stream stream)
    {
        stream.Position = 0L;

        using var hashStream = SHA384.Create();
        var hashBytes = hashStream.ComputeHash(stream);

        // Create a new StringBuilder to collect the bytes and create a string.
        var hashString = new StringBuilder();

        // Loop through each byte of the hashed data and format each one as a hexadecimal string.
        for (var i = 0; i < hashBytes.Length; i++)
        {
            hashString.Append(hashBytes[i].ToString("x2", CultureInfo.InvariantCulture));
        }

        // Return the hexadecimal string.
        return hashString.ToString();
    }

    /// <summary>
    ///   Checks the cache looking for a file with same hash of given <paramref name="file"/>. If it
    ///   finds a file with the same hash, then it deletes specified <paramref name="file"/> and it
    ///   returns the file found in cache. Otherwise, the file is cached and it is returned to the caller.
    /// </summary>
    /// <param name="file">
    ///   File which might be deleted if a file with the same hash exists in the cache.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>
    ///   The original file, if it does not exist in the cache. Otherwise, it returns the cached file.
    /// </returns>
    /// <remarks>This method is used to avoid flooding the server with the same files.</remarks>
    private async Task<TempFileMetadata> CheckCacheAsync(TempFileMetadata file, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(file.Path))
        {
            return file;
        }

        string hash;
        using (var fileStream = File.OpenRead(file.Path))
        {
            hash = ComputeSha384Hash(fileStream);
        }

        var cachedFile = await _cache.GetOrAddSlidingAsync(
            partition: nameof(TempFileMetadata),
            key: new CacheKey(hash),
            valueGetter: () => Task.FromResult(file),
            interval: _databaseConfiguration.Value.CacheLifetime,
            cancellationToken: cancellationToken);

        // A cached file path might not exists, for example, when the container crashes and
        // Thumbnailer temporary directory, if it has not been persisted, is lost.
        if (!File.Exists(cachedFile.Path))
        {
            return file;
        }

        if (cachedFile.Path != file.Path)
        {
            await DeleteTempFileAsync(file, cancellationToken);
        }

        return cachedFile;
    }
}
