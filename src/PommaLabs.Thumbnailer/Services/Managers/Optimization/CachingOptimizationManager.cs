﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Managers.Optimization;

using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using PommaLabs.KVLite;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   Optimization manager which applies caching policies.
/// </summary>
public sealed class CachingOptimizationManager : IOptimizationManager
{
    private readonly ICache _cache;
    private readonly IOptions<DatabaseConfiguration> _databaseConfiguration;
    private readonly IOptimizationManager _optimizationManager;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="optimizationManager">Optimization manager.</param>
    /// <param name="cache">Cache.</param>
    /// <param name="databaseConfiguration">Database configuration.</param>
    public CachingOptimizationManager(
        IOptimizationManager optimizationManager,
        ICache cache,
        IOptions<DatabaseConfiguration> databaseConfiguration)
    {
        _optimizationManager = optimizationManager;
        _cache = cache;
        _databaseConfiguration = databaseConfiguration;
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> OptimizeMediaAsync(MediaOptimizationCommand command, CancellationToken cancellationToken)
    {
        var file = await _cache.GetOrAddSlidingAsync(
            partition: nameof(IOptimizationManager),
            valueExpression: () => _optimizationManager.OptimizeMediaAsync(command, cancellationToken),
            interval: _databaseConfiguration.Value.CacheLifetime,
            cancellationToken: cancellationToken);

        // A cached file path might not exists, for example, when the container crashes and
        // Thumbnailer temporary directory, if it has not been persisted, is lost.
        return File.Exists(file.Path)
            ? file
            : await _optimizationManager.OptimizeMediaAsync(command, cancellationToken);
    }
}
