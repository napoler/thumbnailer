﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Managers.Thumbnail;

using System.IO;
using System.Threading;
using System.Threading.Tasks;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Services.Managers.Optimization;
using PommaLabs.Thumbnailer.Services.Managers.Process;
using PommaLabs.Thumbnailer.Services.Stores.TempFiles;

/// <summary>
///   Thumbnail manager which relies on external processes to generate file thumbnails.
/// </summary>
public sealed class ConcreteThumbnailManager : IThumbnailManager
{
    private readonly SemaphoreSlim _libreOfficeLock = new(1, 1);
    private readonly IOptimizationManager _optimizationManager;
    private readonly IProcessManager _processManager;
    private readonly ITempFileStore _tempFileStore;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="processManager">Command manager.</param>
    /// <param name="tempFileStore">Temporary file store.</param>
    /// <param name="optimizationManager">Optimization manager.</param>
    public ConcreteThumbnailManager(
        IProcessManager processManager,
        ITempFileStore tempFileStore,
        IOptimizationManager optimizationManager)
    {
        _processManager = processManager;
        _tempFileStore = tempFileStore;
        _optimizationManager = optimizationManager;
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> GenerateThumbnailAsync(ThumbnailGenerationCommand command,
        CancellationToken cancellationToken)
    {
        var file = command.File;
        var thumbnail = await _tempFileStore.GetTempFileAsync(MimeTypeMap.IMAGE.PNG, cancellationToken);

        file = await PreprocessFileAsync(command, file, thumbnail, cancellationToken);

        var format = file.ContentType switch
        {
            MimeTypeMap.IMAGE.X_PANASONIC_RAW => "dng:",
            _ => string.Empty
        };

        // Flags which are used both for the standard flow and the smart crop one.
        var commonConvertFlags = $"-auto-orient -density 96 {format}{file.Path}[0] -shave {command.ShavePx}x{command.ShavePx}";

        if (command.SmartCrop)
        {
            var jpeg = await _tempFileStore.GetTempFileAsync(MimeTypeMap.IMAGE.JPEG, cancellationToken);

            // Smart crop utility works best with JPEG images.
            await _processManager.RunProcessAsync(
                "gm", $"convert {commonConvertFlags} -background white -extent 0x0 +matte {jpeg.Path}",
                setTimeout: true, cancellationToken: cancellationToken);

            await _processManager.RunProcessAsync(
                "smartcroppy", $"--width {command.WidthPx} --height {command.HeightPx} {jpeg.Path} {jpeg.Path}",
                setTimeout: true, cancellationToken: cancellationToken);

            await _processManager.RunProcessAsync(
                "gm", $"convert {jpeg.Path} {thumbnail.Path}",
                setTimeout: true, cancellationToken: cancellationToken);
        }
        else
        {
            var background = file.ContentType switch
            {
                MimeTypeMap.IMAGE.PNG => "none",
                _ => "white"
            };

            await _processManager.RunProcessAsync(
                "gm", $"convert {commonConvertFlags} -background {background} -resize {command.WidthPx}x{command.HeightPx} {thumbnail.Path}",
                setTimeout: true, cancellationToken: cancellationToken);

            if (command.Fill)
            {
                await _processManager.RunProcessAsync(
                    "gm", $"convert {thumbnail.Path} -background none -gravity center -extent {command.WidthPx}x{command.HeightPx} {thumbnail.Path}",
                    setTimeout: true, cancellationToken: cancellationToken);
            }
        }

        var moCommand = new MediaOptimizationCommand(thumbnail);
        return await _optimizationManager.OptimizeMediaAsync(moCommand, cancellationToken);
    }

    private async Task<TempFileMetadata> PreprocessFileAsync(ThumbnailGenerationCommand command, TempFileMetadata file, TempFileMetadata thumbnail,
        CancellationToken cancellationToken)
    {
        if (file.IsEbook)
        {
            var pdf = await _tempFileStore.GetTempFileAsync(MimeTypeMap.APPLICATION.PDF, cancellationToken);
            await ConvertEbookToPdfAsync(file, pdf, cancellationToken);
            return pdf;
        }
        if (file.IsHtmlDocument)
        {
            await ConvertHtmlToPngAsync(file, thumbnail, command.WidthPx, cancellationToken);
            return thumbnail;
        }
        if (file.IsOfficeDocument)
        {
            return await ConvertOfficeToPdfAsync(file, cancellationToken);
        }
        if (file.IsSvgDocument)
        {
            await ConvertSvgToPngAsync(file, thumbnail, command.WidthPx, command.HeightPx, cancellationToken);
            return thumbnail;
        }
        if (file.IsTextDocument)
        {
            var html = await _tempFileStore.GetTempFileAsync(MimeTypeMap.TEXT.HTML, cancellationToken);
            await ConvertTextToPngAsync(file, html, thumbnail, command.WidthPx, cancellationToken);
            return thumbnail;
        }
        if (file.IsVideo)
        {
            await ConvertVideoToPngAsync(file, thumbnail, cancellationToken);
            return thumbnail;
        }
        if (file.ContentType == MimeTypeMap.APPLICATION.DICOM)
        {
            await ConvertDicomToPngAsync(file, thumbnail, cancellationToken);
            return thumbnail;
        }
        if (file.ContentType == MimeTypeMap.IMAGE.AVIF)
        {
            await ConvertAvifToPngAsync(file, thumbnail, cancellationToken);
            return thumbnail;
        }
        if (file.ContentType == MimeTypeMap.IMAGE.HEIC)
        {
            var jpeg = await _tempFileStore.GetTempFileAsync(MimeTypeMap.IMAGE.JPEG, cancellationToken);
            await ConvertHeicToJpegAsync(file, jpeg, cancellationToken);
            return jpeg;
        }
        return file;
    }

    private async Task ConvertAvifToPngAsync(TempFileMetadata file, TempFileMetadata png,
        CancellationToken cancellationToken)
    {
        await _processManager.RunProcessAsync(
            "avifdec", $"{file.Path} {png.Path}",
            setTimeout: true, cancellationToken: cancellationToken);
    }

    private async Task ConvertDicomToPngAsync(TempFileMetadata file, TempFileMetadata png,
        CancellationToken cancellationToken)
    {
        await _processManager.RunProcessAsync(
            "dcmj2pnm", $"--quiet --frame 1 --write-png --nointerlace --meta-none {file.Path} {png.Path}",
            setTimeout: true, cancellationToken: cancellationToken);
    }

    private async Task ConvertEbookToPdfAsync(TempFileMetadata file, TempFileMetadata pdf,
        CancellationToken cancellationToken)
    {
        await _processManager.RunProcessAsync(
            "ebook-convert", $"{file.Path} {pdf.Path}",
            setTimeout: true, cancellationToken: cancellationToken);
    }

    private async Task ConvertHeicToJpegAsync(TempFileMetadata file, TempFileMetadata jpeg,
        CancellationToken cancellationToken)
    {
        await _processManager.RunProcessAsync(
            "heif-convert", $"-q 100 {file.Path} {jpeg.Path}",
            setTimeout: true, cancellationToken: cancellationToken);
    }

    private async Task ConvertHtmlToPngAsync(TempFileMetadata file, TempFileMetadata png, int widthPx,
        CancellationToken cancellationToken)
    {
        // Use only HD formats (1080, 2160, ...). widthPx parameter is scaled to HD format in order
        // to obtain a well proportioned screenshot.
        var scaleFactor = (widthPx / 1080) + 1;

        await _processManager.RunProcessAsync(
            "firefox", $"--screenshot {png.Path} --window-size {1080 * scaleFactor} file://{file.Path}",
            setTimeout: true, cancellationToken: cancellationToken);
    }

    private async Task<TempFileMetadata> ConvertOfficeToPdfAsync(TempFileMetadata file,
        CancellationToken cancellationToken)
    {
        try
        {
            await _libreOfficeLock.WaitAsync(cancellationToken);
            await _processManager.RunProcessAsync(
                "libreoffice", $"--convert-to pdf {file.Path}",
                setTimeout: true, cancellationToken: cancellationToken);
        }
        finally
        {
            _libreOfficeLock.Release();
        }
        var extension = Path.GetExtension(file.Path);
        var filePathWithoutExtension = file.Path[..^extension.Length];
        var pdfPath = filePathWithoutExtension + MimeTypeMap.Extensions.PDF;
        return new TempFileMetadata(MimeTypeMap.APPLICATION.PDF) { Path = pdfPath };
    }

    private async Task ConvertSvgToPngAsync(TempFileMetadata file, TempFileMetadata png, int widthPx, int heightPx,
        CancellationToken cancellationToken)
    {
        await _processManager.RunProcessAsync(
            "rsvg-convert", $"{file.Path} --output={png.Path} --width={widthPx} --height={heightPx} --format=png --background-color=none --keep-aspect-ratio --unlimited --no-keep-image-data",
            setTimeout: true, cancellationToken: cancellationToken);
    }

    private async Task ConvertTextToPngAsync(TempFileMetadata file, TempFileMetadata html, TempFileMetadata png, int widthPx,
        CancellationToken cancellationToken)
    {
        // Converts HTML into PNG, which can be easily processed by GraphicsMagick. Size will be
        // proportional to 1240x1754 pixels, so that an A4 PNG at 150 DPI (or more) is produced.
        var scaleFactor = (widthPx / 1240) + 1;

        // - full=true produces an HTML file with embedded CSS.
        // - prestyles=font-size:16px makes fonts big enough to show 120 characters per line.
        // - prestyles=white-space:pre-wrap enables line wrapping when necessary.
        await _processManager.RunProcessAsync(
            "pygmentize", $"-o {html.Path} -O \"full=true,prestyles=font-size:{24 * scaleFactor}px;white-space:pre-wrap,cssstyles=margin:{30 * scaleFactor}px;\" {file.Path}",
            setTimeout: true, cancellationToken: cancellationToken);

        await _processManager.RunProcessAsync(
            "firefox", $" --screenshot {png.Path} --window-size {1240 * scaleFactor},{1754 * scaleFactor} file://{html.Path}",
            setTimeout: true, cancellationToken: cancellationToken);
    }

    private async Task ConvertVideoToPngAsync(TempFileMetadata file, TempFileMetadata png,
        CancellationToken cancellationToken)
    {
        await _processManager.RunProcessAsync(
            "ffmpeg", $"-y -v panic -i {file.Path} -vf thumbnail -frames:v 1 {png.Path}",
            setTimeout: true, cancellationToken: cancellationToken);
    }
}
