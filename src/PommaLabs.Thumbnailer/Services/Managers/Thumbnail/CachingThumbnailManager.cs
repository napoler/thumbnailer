﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Managers.Thumbnail;

using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using PommaLabs.KVLite;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   Thumbnail manager which applies caching policies.
/// </summary>
public sealed class CachingThumbnailManager : IThumbnailManager
{
    private readonly ICache _cache;
    private readonly IOptions<DatabaseConfiguration> _databaseConfiguration;
    private readonly IThumbnailManager _thumbnailManager;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="thumbnailManager">Thumbnail manager.</param>
    /// <param name="cache">Cache.</param>
    /// <param name="databaseConfiguration">Database configuration.</param>
    public CachingThumbnailManager(
        IThumbnailManager thumbnailManager,
        ICache cache,
        IOptions<DatabaseConfiguration> databaseConfiguration)
    {
        _thumbnailManager = thumbnailManager;
        _cache = cache;
        _databaseConfiguration = databaseConfiguration;
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> GenerateThumbnailAsync(ThumbnailGenerationCommand command, CancellationToken cancellationToken)
    {
        var file = await _cache.GetOrAddSlidingAsync(
            partition: nameof(IThumbnailManager),
            valueExpression: () => _thumbnailManager.GenerateThumbnailAsync(command, cancellationToken),
            interval: _databaseConfiguration.Value.CacheLifetime,
            cancellationToken: cancellationToken);

        // A cached file path might not exists, for example, when the container crashes and
        // Thumbnailer temporary directory, if it has not been persisted, is lost.
        return File.Exists(file.Path)
            ? file
            : await _thumbnailManager.GenerateThumbnailAsync(command, cancellationToken);
    }
}
