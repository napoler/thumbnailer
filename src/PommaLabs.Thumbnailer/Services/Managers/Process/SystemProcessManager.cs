﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Managers.Process;

using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.Exceptions;

/// <summary>
///   Concrete <see cref="IProcessManager"/> implementation.
/// </summary>
public sealed class SystemProcessManager : IProcessManager
{
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly ILogger<SystemProcessManager> _logger;
    private readonly IOptions<SecurityConfiguration> _securityConfiguration;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="logger">Logger.</param>
    /// <param name="securityConfiguration">Security configuration.</param>
    /// <param name="httpContextAccessor">HTTP context accessor.</param>
    public SystemProcessManager(
        ILogger<SystemProcessManager> logger,
        IOptions<SecurityConfiguration> securityConfiguration,
        IHttpContextAccessor httpContextAccessor)
    {
        _logger = logger;
        _securityConfiguration = securityConfiguration;
        _httpContextAccessor = httpContextAccessor;
    }

    /// <inheritdoc/>
    public async Task RunProcessAsync(string processName, string processArgs, bool setTimeout, CancellationToken cancellationToken)
    {
        // When we use evaluator helper script, the real process name is the first word of the arguments.
        var realProcessName = processName == "evaluator" ? processArgs.Split(' ')[0] : processName;

        _logger.LogInformation("{ProcessName} process will be run with following arguments: {ProcessArgs}", realProcessName, processArgs);

        using var process = Process.Start(new ProcessStartInfo(processName, processArgs)
        {
            WorkingDirectory = Constants.TempDirectory,
            UseShellExecute = false,
            RedirectStandardError = true,
        });

        if (process == null)
        {
            throw new ProcessException($"External process {realProcessName} failed to start", realProcessName, default);
        }

        if (setTimeout)
        {
            await WaitForExitWithTimeoutAsync(process, realProcessName, processArgs, cancellationToken);
        }
        else
        {
            await WaitForExitWithoutTimeoutAsync(process, cancellationToken);
        }

        if (process.ExitCode != 0)
        {
            var processError = await process.StandardError.ReadToEndAsync();

            _logger.LogError("{ProcessName} exited with code {ExitCode}. It was started with following arguments: {ProcessArgs}. Process error is: {ProcessError}", realProcessName, process.ExitCode, processArgs, processError);
            throw new ProcessException($"External process {realProcessName} raised an error", realProcessName, process.ExitCode);
        }
    }

    private static async Task WaitForExitWithoutTimeoutAsync(Process process, CancellationToken cancellationToken)
    {
        try
        {
            await process.WaitForExitAsync(cancellationToken);
        }
        catch (OperationCanceledException)
        {
            process.Kill();
            throw;
        }
    }

    private async Task WaitForExitWithTimeoutAsync(Process process, string processName, string processArgs, CancellationToken cancellationToken)
    {
        var timeout = _httpContextAccessor.HttpContext != null
            ? (int)_securityConfiguration.Value.ProcessTimeout.TotalMilliseconds
            : (int)_securityConfiguration.Value.AsyncProcessTimeout.TotalMilliseconds;

        if (!process.WaitForExit(timeout))
        {
            try
            {
                process.Kill();

                _logger.LogWarning("A timeout occurred while running {ProcessName} with following arguments: {ProcessArgs}. Configured timeout is: {Timeout}", processName, processArgs, timeout);
                throw new ProcessException($"External process {processName} exceeded the timeout of {timeout} milliseconds and it was killed", processName, process.ExitCode);
            }
            catch (InvalidOperationException)
            {
                // The process already finished by itself.
                await process.WaitForExitAsync(cancellationToken);
            }
        }
    }
}
