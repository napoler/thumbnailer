﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Managers.Download;

using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using PommaLabs.KVLite;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   Download manager which applies caching policies.
/// </summary>
public sealed class CachingDownloadManager : IDownloadManager
{
    private readonly ICache _cache;
    private readonly IOptions<DatabaseConfiguration> _databaseConfiguration;
    private readonly IDownloadManager _downloadManager;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="downloadManager">Download manager.</param>
    /// <param name="cache">Cache.</param>
    /// <param name="databaseConfiguration">Database configuration.</param>
    public CachingDownloadManager(
        IDownloadManager downloadManager,
        ICache cache,
        IOptions<DatabaseConfiguration> databaseConfiguration)
    {
        _downloadManager = downloadManager;
        _cache = cache;
        _databaseConfiguration = databaseConfiguration;
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> DownloadFileAsync(Uri fileUri, CancellationToken cancellationToken)
    {
        var file = await _cache.GetOrAddSlidingAsync(
            partition: nameof(IDownloadManager),
            valueExpression: () => _downloadManager.DownloadFileAsync(fileUri, cancellationToken),
            interval: _databaseConfiguration.Value.CacheLifetime,
            cancellationToken: cancellationToken);

        // A cached file path might not exists, for example, when the container crashes and
        // Thumbnailer temporary directory, if it has not been persisted, is lost.
        return File.Exists(file.Path)
            ? file
            : await _downloadManager.DownloadFileAsync(fileUri, cancellationToken);
    }
}
