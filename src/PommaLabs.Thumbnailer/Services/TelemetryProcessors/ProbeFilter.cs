﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.TelemetryProcessors;

using System;
using Microsoft.ApplicationInsights.Channel;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.AspNetCore.Http;
using PommaLabs.Thumbnailer.Core;

/// <summary>
///   Removes endpoints used for probes from Application Insights telemetry.
/// </summary>
public sealed class ProbeFilter : ITelemetryProcessor
{
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly ITelemetryProcessor _next;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="next">Next processor in the chain.</param>
    /// <param name="httpContextAccessor">HTTP context accessor.</param>
    public ProbeFilter(
        ITelemetryProcessor next,
        IHttpContextAccessor httpContextAccessor)
    {
        _next = next;
        _httpContextAccessor = httpContextAccessor;
    }

    /// <inheritdoc/>
    public void Process(ITelemetry item)
    {
        var requestPath = _httpContextAccessor.HttpContext?.Request?.Path;
        if (!string.Equals(requestPath, Constants.HealthChecksEndpoint, StringComparison.OrdinalIgnoreCase)
            && !string.Equals(requestPath, Constants.VersionEndpoint, StringComparison.OrdinalIgnoreCase))
        {
            _next.Process(item);
        }
    }
}
