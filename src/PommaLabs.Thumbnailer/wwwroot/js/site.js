﻿"use strict";

// Application data:
window.appData = {
    apiKey: "",
    operationType: ""
};

// Dropzone configuration:
Dropzone.options.thumbnailerDropzone = {
    // Prevents Dropzone from uploading dropped files immediately.
    autoProcessQueue: false,

    // Maximum number of files which can be uploaded.
    maxFiles: 20,
    parallelUploads: 20,

    // Maximum file size for each upload.
    maxFilesize: 50,

    url: function () {
        return $("#thumbnailer-dropzone").prop("action");
    },

    init: function () {
        window.appData.thumbnailerDropzone = this;

        this.on("addedfile", function () {
            // Show submit button here and/or inform user to click it.
            enableRecaptchaApiKeySubmitButton();
        });

        this.on("sending", function (__file, xhr, __formData) {
            disableRecaptchaApiKeySubmitButton();
            xhr.setRequestHeader("X-Api-Key", window.appData.apiKey);
        });

        this.on("success", function (file, response) {
            const ext = (window.appData.operationType === "ThumbnailGeneration") ? ".png" : "";
            window.appData.outputZip.file(file.name + ext, response.contents, { base64: true });
        });

        this.on("queuecomplete", function () {
            window.appData.outputZip.generateAsync({ type: "blob" })
                .then(function (content) {
                    saveAs(content, ~~(Date.now() / 1000) + ".zip");
                });

            this.removeAllFiles(true);
            grecaptcha.reset();
            enableRecaptchaApiKeySubmitButton();
        });
    }
};
