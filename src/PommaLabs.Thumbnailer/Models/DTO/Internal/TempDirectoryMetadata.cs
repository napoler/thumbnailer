﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Models.DTO.Internal;

using System;
using System.IO;

/// <summary>
///   Represents a temporary directory.
/// </summary>
public sealed class TempDirectoryMetadata : IDisposable
{
    /// <summary>
    ///   Directory path on local file system.
    /// </summary>
    public string Path { get; set; } = string.Empty;

    /// <inheritdoc/>
    public void Dispose()
    {
        if (!string.IsNullOrWhiteSpace(Path) && Directory.Exists(Path))
        {
            Directory.Delete(Path, recursive: true);
            Path = string.Empty;
        }
    }
}
