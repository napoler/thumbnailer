﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Models.Configurations;

using System;
using System.Collections.Generic;
using Newtonsoft.Json;

/// <summary>
///   Security configuration.
/// </summary>
public sealed class SecurityConfiguration
{
    private List<ApiKeyConfiguration> _apiKeys = new();
    private TimeSpan _asyncProcessTimeout = TimeSpan.FromMinutes(10);
    private TimeSpan _fileDownloadTimeout = TimeSpan.FromSeconds(30);
    private long _maxFileDownloadSizeInBytes = 512 * 1024 * 1024;
    private long _maxFileUploadSizeInBytes = 256 * 1024 * 1024;
    private TimeSpan _processTimeout = TimeSpan.FromSeconds(30);

    /// <summary>
    ///   Accept API keys via header parameter. Defaults to true.
    /// </summary>
    public bool AcceptApiKeysViaHeaderParameter { get; set; } = true;

    /// <summary>
    ///   Accept API keys via query string parameter. Defaults to false, because, from a security
    ///   point of view, it is better to specify API keys as header parameter.
    /// </summary>
    public bool AcceptApiKeysViaQueryStringParameter { get; set; }

    /// <summary>
    ///   Allow anonymous users to consume services. Defaults to false, because anonymous users can
    ///   overload the web service, unless a rate limiting process is active.
    /// </summary>
    public bool AllowAnonymousAccess { get; set; }

    /// <summary>
    ///   Allowed API keys.
    /// </summary>
    public List<ApiKeyConfiguration> ApiKeys
    {
        get => _apiKeys;
        set => _apiKeys = value ?? _apiKeys;
    }

    /// <summary>
    ///   Allowed API keys. This is a configuration shortcut for <see cref="ApiKeys"/>,
    ///   useful when many API keys need to be specified using environment variables.
    ///   This property allows setting one environment variable with a JSON array
    ///   of desired API keys, so that they can be parsed and mapped to <see cref="ApiKeys"/>.
    /// </summary>
    [Obsolete("See summary", error: true)]
    public string ApiKeysJson
    {
        get => JsonConvert.SerializeObject(ApiKeys);
        set => _apiKeys = JsonConvert.DeserializeObject<List<ApiKeyConfiguration>>(value) ?? new List<ApiKeyConfiguration>();
    }

    /// <summary>
    ///   How long low level processes (e.g. "gm" calls) executed from asynchronous jobs should last
    ///   before being interrupted. Defaults to 10 minutes and it cannot be greater than one hour.
    /// </summary>
    public TimeSpan AsyncProcessTimeout
    {
        get => _asyncProcessTimeout;
        set => _asyncProcessTimeout = (value > TimeSpan.FromMinutes(60))
            ? throw new ArgumentOutOfRangeException(nameof(value), "Asynchronous process timeout must be less than or equal to one hour")
            : value;
    }

    /// <summary>
    ///   API keys are validated using a case sensitive comparer. Defaults to false, because it was
    ///   how the web service initially behaved. Please set this to true on new installations.
    /// </summary>
    public bool EnableCaseSensitiveApiKeyValidation { get; set; }

    /// <summary>
    ///   How long file download should last before being interrupted. Defaults to 30 seconds and it
    ///   cannot be greater than 10 minutes.
    /// </summary>
    public TimeSpan FileDownloadTimeout
    {
        get => _fileDownloadTimeout;
        set => _fileDownloadTimeout = (value > TimeSpan.FromMinutes(10))
            ? throw new ArgumentOutOfRangeException(nameof(value), "File download timeout must be less than or equal to ten minutes")
            : value;
    }

    /// <summary>
    ///   How many bytes are allowed for remote downloads. Defaults to 64 MB.
    /// </summary>
    public long MaxFileDownloadSizeInBytes
    {
        get => _maxFileDownloadSizeInBytes;
        set => _maxFileDownloadSizeInBytes = (value <= 0L)
            ? throw new ArgumentOutOfRangeException(nameof(value), "Max file download size must be greater than zero")
            : value;
    }

    /// <summary>
    ///   How many bytes are allowed for uploads. Defaults to 32 MB.
    /// </summary>
    public long MaxFileUploadSizeInBytes
    {
        get => _maxFileUploadSizeInBytes;
        set => _maxFileUploadSizeInBytes = (value <= 0L)
            ? throw new ArgumentOutOfRangeException(nameof(value), "Max file upload size must be greater than zero")
            : value;
    }

    /// <summary>
    ///   How long low level processes (e.g. "gm" calls) executed from synchronous requests should
    ///   last before being interrupted. Defaults to 30 seconds and it cannot be greater than 10 minutes.
    /// </summary>
    public TimeSpan ProcessTimeout
    {
        get => _processTimeout;
        set => _processTimeout = (value > TimeSpan.FromMinutes(10))
            ? throw new ArgumentOutOfRangeException(nameof(value), "Process timeout must be less than or equal to ten minutes")
            : value;
    }
}
