﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Models.Configurations;

/// <summary>
///   Job processor configuration.
/// </summary>
public sealed class JobProcessorConfiguration
{
    /// <summary>
    ///   How many job processor threads should be spawned. Defaults to 2.
    /// </summary>
    public int Count { get; set; } = 2;
}
