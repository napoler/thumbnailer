﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.IO;

namespace PommaLabs.Thumbnailer.Core;

/// <summary>
///   Application constants.
/// </summary>
public static class Constants
{
    /// <summary>
    ///   Anonymous authentication scheme.
    /// </summary>
    public const string AnonymousAuthenticationScheme = "Anonymous";

    /// <summary>
    ///   Anonymous identity name.
    /// </summary>
    public const string AnonymousIdentityName = "Anonymous";

    /// <summary>
    ///   API key header name.
    /// </summary>
    public const string ApiKeyHeaderName = "X-Api-Key";

    /// <summary>
    ///   API key query parameter name.
    /// </summary>
    public const string ApiKeyQueryParameterName = "apiKey";

    /// <summary>
    ///   API key via header authentication scheme.
    /// </summary>
    public const string ApiKeyViaHeaderAuthenticationScheme = "API key via header";

    /// <summary>
    ///   API key via query parameter authentication scheme.
    /// </summary>
    public const string ApiKeyViaQueryParameterAuthenticationScheme = "API key via query parameter";

    /// <summary>
    ///   Health checks endpoint.
    /// </summary>
    public const string HealthChecksEndpoint = "/health";

    /// <summary>
    ///   "multipart/form-data" content type header.
    /// </summary>
    public const string MultipartFormData = "multipart/form-data";

    /// <summary>
    ///   Version endpoint.
    /// </summary>
    public const string VersionEndpoint = "/version";

    /// <summary>
    ///   Port on which Thumbnailer service listens to.
    /// </summary>
    public static string Port { get; set; } = "8080";

    /// <summary>
    ///   Where application temporary files are stored.
    /// </summary>
    public static string TempDirectory { get; } = Path.Combine(Path.GetTempPath(), "thumbnailer");

    /// <summary>
    ///   Activity names used for telemetry.
    /// </summary>
    public static class ActivityNames
    {
        /// <summary>
        ///   Media optimization.
        /// </summary>
        public const string MediaOptimization = "media_optimization";

        /// <summary>
        ///   Thumbnail generation.
        /// </summary>
        public const string ThumbnailGeneration = "thumbnail_generation";
    }

    /// <summary>
    ///   Tag names used for telemetry.
    /// </summary>
    public static class TagNames
    {
        /// <summary>
        ///   File content type of a command.
        /// </summary>
        public const string CommandFileContentType = "command.file_content_type";

        /// <summary>
        ///   Internal job ID of a command.
        /// </summary>
        public const string CommandInternalJobId = "command.internal_job_id";
    }
}
