﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.IntegrationTests;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using ImageMagick;
using Newtonsoft.Json;
using NUnit.Framework;
using PommaLabs.Thumbnailer.Client;
using PommaLabs.Thumbnailer.Client.Core;
using PommaLabs.Thumbnailer.Client.Models.Enumerations;
using VerifyTests;

[TestFixture, Parallelizable]
internal abstract class ThumbnailerTestsBase
{
    protected const string ApiKeyHeaderName = "X-Api-Key";

    private const string SampleFilesDirectory = "SampleFiles";
    private const string SpecFileName = "_spec.json";

    static ThumbnailerTestsBase()
    {
        const double Threshold = 0.05;
        const ErrorMetric Metric = ErrorMetric.Fuzz;
        VerifyImageMagick.RegisterComparer(Threshold, Metric, "gif");
        VerifyImageMagick.RegisterComparer(Threshold, Metric, "jpg");
        VerifyImageMagick.RegisterComparer(Threshold, Metric, "png");
        VerifyImageMagick.RegisterComparer(Threshold, Metric, "webp");
    }

    protected static async Task EnsureServiceIsHealthyAsync(IThumbnailerClient client)
    {
        const int RetryCount = 30;
        const int Delay = 1000;

        for (var i = 0; i < RetryCount; ++i)
        {
            if (await client!.IsHealthyAsync())
            {
                return;
            }
            await Task.Delay(Delay);
        }

        throw new InvalidOperationException("Thumbnailer service is not healthy, tests will not be run");
    }

    protected static IEnumerable<SampleFile> ReadSampleFiles(OperationType operationType)
    {
        var sampleFiles = new ConcurrentBag<SampleFile>();

        var randomSample = bool.Parse(Environment.GetEnvironmentVariable("THUMBNAILER_RANDOM_SAMPLE") ?? "false");
        var random = new Random();

        foreach (var fileFormatDirectory in Directory.GetDirectories(SampleFilesDirectory))
        {
            var specFile = ReadSpecFile(fileFormatDirectory);

            if ((operationType == OperationType.ThumbnailGeneration && !specFile.ThumbnailGenerationSupported) ||
                (operationType == OperationType.MediaOptimization && !specFile.MediaOptimizationSupported))
            {
                continue;
            }

            var filePaths = Directory
                .GetFiles(fileFormatDirectory)
                .Where(fp => !fp.EndsWith(SpecFileName));

            if (randomSample)
            {
                var tempFilePaths = filePaths.ToList();
                var sampleIndex = random.Next(tempFilePaths.Count);
                filePaths = new[] { tempFilePaths[sampleIndex] };
            }

            Parallel.ForEach(filePaths, filePath =>
            {
                sampleFiles.Add(new SampleFile
                {
                    LocalPath = filePath,
                    Format = Path.GetFileName(fileFormatDirectory).ToUpperInvariant(),
                    ThumbnailGenerationSupported = specFile.ThumbnailGenerationSupported,
                    MediaOptimizationSupported = specFile.MediaOptimizationSupported
                }); ;
            });
        }

        return sampleFiles;
    }

    protected VerifySettings CreateVerifySettings(SampleFile sampleFile, string? format = null, [CallerMemberName] string? methodName = null)
    {
        var testContext = methodName!.Split("_")[1];

        var settings = new VerifySettings();

        settings.UseDirectory("VerifiedResults");
        settings.UseFileName($"{GetType().Name}.{testContext}.{sampleFile.Code}");
        settings.UseExtension((format ?? sampleFile.Format).ToLowerInvariant());

        // Verified results are shared between tests for v1 and v2 endpoints.
        settings.DisableRequireUniquePrefix();

        return settings;
    }

    private static SpecFile ReadSpecFile(string fileFormatDirectory)
    {
        var specFileContents = File.ReadAllText(Path.Combine(fileFormatDirectory, SpecFileName));
        return JsonConvert.DeserializeObject<SpecFile>(specFileContents)!;
    }

    public sealed class SampleFile : SpecFile
    {
        public string Code => $"{Format}_{Number}";

        public string ContentType => MimeTypeHelper.GetMimeTypeAndExtension(LocalPath).mimeType;

        public string Format { get; set; } = string.Empty;

        public string LocalPath { get; set; } = string.Empty;

        public string Name => Path.GetFileName(LocalPath);

        public string Number => Path.GetFileNameWithoutExtension(LocalPath);

        public Uri RemoteUri => new($"https://gitlab.com/pommalabs/thumbnailer/-/raw/preview/tests/PommaLabs.Thumbnailer.IntegrationTests/SampleFiles/{Format.ToLowerInvariant()}/{Name}?inline=false");

        public override string ToString() => Code;
    }

    public class SpecFile
    {
        public bool MediaOptimizationSupported { get; set; }

        public bool ThumbnailGenerationSupported { get; set; }
    }
}
