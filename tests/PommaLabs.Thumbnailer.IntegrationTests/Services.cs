﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.IntegrationTests;

using System;
using System.Net.Http;
using Flurl.Http;
using Flurl.Http.Configuration;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using PommaLabs.Thumbnailer.Client;

[SetUpFixture]
internal static class Services
{
    private static readonly string? s_baseUri;
    private static readonly WebApplicationFactory<Startup>? s_webServiceFactory;

    static Services()
    {
        var services = new ServiceCollection();

        s_baseUri = Environment.GetEnvironmentVariable("THUMBNAILER_BASE_URI");

        services.Configure<ThumbnailerClientConfiguration>(options =>
        {
            var apiKey = Environment.GetEnvironmentVariable("THUMBNAILER_API_KEY");

            if (s_baseUri != null) options.BaseUri = new Uri(s_baseUri);
            if (apiKey != null) options.ApiKey = apiKey;
        });

        services.AddLogging(options => options.AddConsole().SetMinimumLevel(LogLevel.Trace));

        services.AddThumbnailerClient();

        ServiceProvider = services.BuildServiceProvider();

        if (string.IsNullOrWhiteSpace(s_baseUri))
        {
            s_webServiceFactory = new WebApplicationFactory<Startup>();
            FlurlHttp.Configure(settings => settings.HttpClientFactory = new CustomHttpClientFactory(s_webServiceFactory));
        }
    }

    public static ServiceProvider ServiceProvider { get; }

    public static HttpClient GetHttpClient()
    {
        return s_webServiceFactory?.CreateClient() ?? new HttpClient { BaseAddress = new Uri(s_baseUri!) };
    }

    [OneTimeTearDown]
    public static void OneTimeTearDown()
    {
        s_webServiceFactory?.Dispose();
        ServiceProvider?.Dispose();
    }

    private sealed class CustomHttpClientFactory : DefaultHttpClientFactory
    {
        private readonly WebApplicationFactory<Startup> _webServiceFactory;

        public CustomHttpClientFactory(WebApplicationFactory<Startup> webServiceFactory)
        {
            _webServiceFactory = webServiceFactory;
        }

        public override HttpClient CreateHttpClient(HttpMessageHandler handler)
        {
            return _webServiceFactory.CreateClient();
        }
    }
}
