# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [4.4.0] - 2022-08-27

### Added

- [Server] Added new `Security__ApiKeysJson` configuration, which can be used
  to define multiple API keys with one configuration entry only.

### Changed

- [Server] Switched from `chromium` to `firefox` to generate thumbnails of HTML files.
- [Server] LibreOffice is used directly as a command line tool. Long running `unoserver`
  processes where consuming too much memory.

### Removed

- [Server] Dropped support for JWT authentication.

## [4.3.0] - 2022-07-31

### Added

- [Server] Added thumbnail generation support for AVIF and HEIC files.
- [Server] Azure Application Insights connection string can be used to configure related sink.

### Changed

- [Server] Number of job processor threads and `unoserver` processes is no longer tied to
  server processor count. `JobProcessor__Count` configuration has been introduced
  in order to control how many threads and processes are created. Configuration value defaults to 2.
- [Server] Logs are no longer written to file system.
- [Server] Data protection keys have been moved to `/tmp/thumbnailer/data-protection-keys`.

## [4.2.0] - 2021-12-19

### Changed

- [Server] Switched to `unoserver` to trigger conversions handled by LibreOffice.
- [Server] v1 endpoints rely on the same job queue used by v2 ones.
- [Server] Added integration tests for v1 endpoints.

## [4.1.0] - 2021-11-21

### Changed

- [Client] Client currently targets: .NET Core 3.1 LTS, .NET 6 LTS, .NET Framework 4.6.1 and 4.7.2.
- [Client] Client will support only .NET LTS releases and supported .NET Framework 4.x releases.
- [Server] Updated .NET to version 6.
- [Server] Updated Debian to version 11.
- [Server] PhantomJS is no longer available on Debian 11. HTML to PNG conversion has been replaced with
  `capture-website-cli`, which relies on Puppeteer and Google Chrome (headless).

## [4.0.2] - 2021-08-20

### Changed

- [Client] Improved handling of unknown mime types.
- [Server] Improved handling of unknown mime types.

## [4.0.1] - 2021-08-19

### Added

- [Server] Implemented Content Security Policy headers.

## [4.0.0] - 2021-08-15

### Added

- [Client] .NET activities are triggered for each interaction with the back-end.
- [Server] Added a new health check which fails if available free space for temporary files drops below 1 MB.

### Changed

- [Server] API keys are validated using constant time comparison.
- [Server] API keys validation can be made case sensitive setting `Security__EnableCaseSensitiveApiKeyValidation` to `true`.

### Removed

- [Client] `Validator` and `MimeTypeHelper` classes are no longer public.
- [Server] `Security__SanitizeHtmlDocuments` configuration is no longer available.

## [3.1.0] - 2021-01-24

### Added

- Client contains an health check for Thumbnailer service.
- `JobTimeout` configuration is available in client configuration, it controls how long client waits for async jobs to be completed.
- `Security__AsyncProcessTimeout` is available to configure timeouts for processes executed from async jobs.

### Changed

- Client now uses v2 endpoints, which trigger async jobs.
- Renamed `Security__CommandTimeout` configuration to `Security__ProcessTimeout`.

## [3.0.0] - 2020-11-08

### Added

- Optimization methods now handle MP4 files.

### Changed

- `GetThumbnailAsync` client methods were renamed into `GenerateThumbnailAsync`.
- `OptimizeImageAsync` client methods were renamed into `OptimizeMediaAsync`.
- `IsImageOptimizationSupportedAsync` client method was renamed into `IsMediaOptimizationSupportedAsync`.

### Removed

- RapidAPI support has been removed. I am deeply sorry, but the quality of that service is really low.

## [2.1.0] - 2020-09-06

### Changed

- Content type might not be specified (library will try to deduce it from file signature).

## [2.0.2] - 2020-06-17

### Changed

- Embedded package icon and license.

## [2.0.1] - 2020-06-05

### Removed

- `ImageOptimizationMode` was added by mistake and it has been removed.

## [2.0.0] - 2020-06-01

### Added

- Added new `fill` parameter to thumbnail generation.
- Implemented a retry policy with exponential backoff on the client. Retry count is configurable.
- Client supports RapidAPI endpoints.

### Changed

- `sidePx` parameter has been replaced with `widthPx` and `heightPx`.

### Removed

- Thumbnailer processor has been removed.

## [1.7.2] - 2020-04-15

### Changed

- On Linux, processor now uses the [`fd` command](https://github.com/sharkdp/fd) to detect modified files. 
  `find` was too slow on directories with many files.

## [1.7.1] - 2020-04-13

### Changed

- On Linux, processor now uses the `find` command to detect modified files.

## [1.7.0] - 2020-04-12

### Changed

- `GenerateFileThumbnails` property of `ProcessableDirectoryConfiguration` class has been renamed into `GenerateThumbnails`.
- Processor operations now return a list of results for each processed directory and file.

## [1.6.3] - 2020-03-29

### Added

- Added custom `MimeTypeMap` with more managed mime types and extensions.

### Changed

- Client now targets .NET Standard 2.0, .NET Framework 4.6.1 and 4.7.2.

## [1.6.2] - 2020-03-16

### Fixed

- JPEG images were discarded by the optimization flow.

## [1.6.1] - 2020-03-15

### Fixed

- Processor lock could not be properly released in a concurrent scenario.

## [1.6.0] - 2020-02-29

### Changed

- Updated NuGet dependencies.
- Client is built for .NET Standard 2.1 and .NET Framework 4.7.2.

[4.4.0]: https://gitlab.com/pommalabs/thumbnailer/-/compare/4.3.0...4.4.0
[4.3.0]: https://gitlab.com/pommalabs/thumbnailer/-/compare/4.2.0...4.3.0
[4.2.0]: https://gitlab.com/pommalabs/thumbnailer/-/compare/4.1.0...4.2.0
[4.1.0]: https://gitlab.com/pommalabs/thumbnailer/-/compare/4.0.2...4.1.0
[4.0.2]: https://gitlab.com/pommalabs/thumbnailer/-/compare/4.0.1...4.0.2
[4.0.1]: https://gitlab.com/pommalabs/thumbnailer/-/compare/4.0.0...4.0.1
[4.0.0]: https://gitlab.com/pommalabs/thumbnailer/-/compare/3.1.0...4.0.0
[3.1.0]: https://gitlab.com/pommalabs/thumbnailer/-/compare/3.0.0...3.1.0
[3.0.0]: https://gitlab.com/pommalabs/thumbnailer/-/compare/2.1.0...3.0.0
[2.1.0]: https://gitlab.com/pommalabs/thumbnailer/-/compare/2.0.2...2.1.0
[2.0.2]: https://gitlab.com/pommalabs/thumbnailer/-/compare/2.0.1...2.0.2
[2.0.1]: https://gitlab.com/pommalabs/thumbnailer/-/compare/2.0.0...2.0.1
[2.0.0]: https://gitlab.com/pommalabs/thumbnailer/-/compare/1.7.2...2.0.0
[1.7.2]: https://gitlab.com/pommalabs/thumbnailer/-/compare/1.7.1...1.7.2
[1.7.1]: https://gitlab.com/pommalabs/thumbnailer/-/compare/1.7.0...1.7.1
[1.7.0]: https://gitlab.com/pommalabs/thumbnailer/-/compare/1.6.3...1.7.0
[1.6.3]: https://gitlab.com/pommalabs/thumbnailer/-/compare/1.6.2...1.6.3
[1.6.2]: https://gitlab.com/pommalabs/thumbnailer/-/compare/1.6.1...1.6.2
[1.6.1]: https://gitlab.com/pommalabs/thumbnailer/-/compare/1.6.0...1.6.1
[1.6.0]: https://gitlab.com/pommalabs/thumbnailer/-/compare/1.5.7...1.6.0
